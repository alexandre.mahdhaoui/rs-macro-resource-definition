extern crate proc_macro;
use proc_macro::{TokenStream};

use syn::{parse_macro_input, ItemStruct, Fields, DeriveInput, FieldsNamed, Data, DataEnum, DataUnion, FieldsUnnamed};
use quote::quote;

#[proc_macro_derive(Describe)]
pub fn describe(input: TokenStream) -> TokenStream {
    let DeriveInput { ident, data, .. } = parse_macro_input!(input);

    let description = match data {
        Data::Struct(s) => match s.fields {
            Fields::Named(FieldsNamed { named, .. }) => {
                let idents = named.iter().map(|f| &f.ident);
                format!(
                    "a struct with these named fields: {}",
                    quote! {#(#idents), *}
                )
            }
            Fields::Unnamed(FieldsUnnamed { unnamed, .. }) => {
                let num_fields = unnamed.iter().count();
                format!("a struct with {} unnamed fields", num_fields)
            }
            Fields::Unit => format!("a unit struct"),
        },
        Data::Enum(DataEnum { variants, .. }) => {
            let vs = variants.iter().map(|v| &v.ident);
            format!("an enum with these variants: {}", quote! {#(#vs),*})
        }
        Data::Union(DataUnion {
                             fields: FieldsNamed { named, .. },
                             ..
                         }) => {
            let idents = named.iter().map(|f| &f.ident);
            format!("a union with these named fields: {}", quote! {#(#idents),*})
        }
    };

    let output = quote! {
        impl #ident {
            fn describe() {
            println!("{} is {}.", stringify!(#ident), #description);
            }
        }
    };

    output.into()
}
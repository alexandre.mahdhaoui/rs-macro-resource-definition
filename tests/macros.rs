use std::collections::HashMap;
use rs_lib_resource_definition::{Describe};

#[test]
fn positive() {

    #[derive(Describe)]
    struct Yolo {
        field_0: String,
        field_1: String,
        fields_2: HashMap<String, String>,
        field_3: Box<Self>,
    }
    Yolo::describe()
}

